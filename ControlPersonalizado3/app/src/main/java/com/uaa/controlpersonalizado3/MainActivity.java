package com.uaa.controlpersonalizado3;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SelectorColores ctlColor;
    private TextView mensaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.ctlColor = (SelectorColores) findViewById(R.id.scColor);
        this.mensaje = (TextView) findViewById(R.id.mensajeColor);

        ctlColor.setOnColorSelectedListener(new SelectorColores.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                StringBuffer colorSeleccionado = new StringBuffer("Color Seleccionado: ");
                switch (color) {
                    case Color.RED:
                        colorSeleccionado.append("Rojo");
                        break;
                    case Color.GREEN:
                        colorSeleccionado.append("Verde");
                        break;
                    case Color.BLUE:
                        colorSeleccionado.append("Azul");
                        break;
                    case Color.YELLOW:
                        colorSeleccionado.append("Amarillo");
                        break;
                }
                mensaje.setText(colorSeleccionado);
            }
        });



    }
}
