package com.uaa.listas;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String[] programas =
            {"Arrow", "The Flash",
                    "The Walking Death",
                    "Breaking bad",
                    "The Big Bang Theory",
                    "Gotham",
                    "Lie to me"};
    private Context context;
    private ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //usamos activity_main.xml como interfaz
        // grafica
        setContentView(R.layout.activity_main);

        //obtenemos el contexto de la aplicacion
        this.context = MainActivity.this;

        //Creamos un adaptador para
        // nuestra lista, el constructor necesita
        //como parametros el contexto de la aplicacion,
        //el layout en el que mostrará
        //y el contenido que tendrá el adaptador
        final ArrayAdapter<String> adaptadorFilaGenerica =
                new ArrayAdapter<>(this.context,
                        android.R.layout.simple_list_item_1,
                        this.programas);

        //Creamos un adaptador
        //utilizando nuestra fila
        //customizada en mi_fila.xml
        //el constructor necesita como
        //parametros el layout y el id del
        //TextView dentro de
        //el layout.
        ListAdapter adaptadorConFilaCustomizada =
                new ArrayAdapter<>(this.context,
                        R.layout.mi_fila, R.id.textView1,
                        this.programas);

        //Creamos un adaptador propio creado por nosotros en MiAdaptador.java
        ListAdapter adaptadorPropio =
                new MiAdaptador(this.context,
                        0, this.programas);

        //Hacemos referencia a
        // la lista declarada en activity_main.xml
        this.lista =
                (ListView) findViewById(R.id.listView);

        //Seleccionamos el adaptador que tendra
        // nuestra lista
        this.lista.setAdapter(adaptadorFilaGenerica);
        //this.lista.setAdapter(adaptadorConFilaCustomizada);
        //this.lista.setAdapter(adaptadorPropio);

        EditText editText = (EditText) findViewById(R.id.filter);
        assert editText != null;
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                adaptadorFilaGenerica.getFilter().filter(text);
            }
        });

        //Agregamos un escuchador a la
        //lista, cada vez que cualquier elemento
        //de la lista sea seleccionada el
        //metodo onItemClick será llamado
        //automaticamente
        this.lista.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?>
                                                    parent, View view,
                                            int position, long id) {

                        //Mostramos una notificacion Toast
                        // con la opcion seleccionada
                        Toast.makeText(context,
                                "Has seleccionado: "
                                        + programas[position],
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
