package com.uaa.listas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MiAdaptador
        extends ArrayAdapter<String> {

    //variable en la que almacenaremos los
    // programas de tv que se mostraran
    // en la lista
    private final String[] programas;

    //Clase estatica, ViewHolder es un patron
    // de diseño para listas
    //que nos permite ahorrar recursos,
    //reutilizando vistas en
    //las celdas ya creadas anteriormente
    static class ViewHolder {
        //Dentro del ViewHolder guardaremos
        //el textView dentro del layout utilizado
        //por el adaptador
        public TextView tv_programa;

        //Constructor, recive el TextView
        //a guardar dentro del layout utilizado
        public ViewHolder(TextView programa) {
            this.tv_programa = programa;
        }

    }

    //LayoutInflater, sirve para "inflar"
    //la ui en un layout declarado en un layout xml
    //"inflar" es el termino utilizado
    //para "convertir" un xml, a objetos java
    private final LayoutInflater li;

    //Constructor recibe, el contexto de la aplicacion,
    public MiAdaptador(Context context, int resource,
                       String[] objects) {
        super(context, resource, objects);
        //guardamos el contenido de la lista
        //en el atributo programas
        this.programas = objects;
        //obtenemos el servicio "Layout infater"
        //el cual "infla" nuestra UI
        //dentro de un xml y convierte este
        // cotenido en objetos java
        this.li = (LayoutInflater) context
             .getSystemService(
                     Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * El metodo getView es llamado
     * automaticamente por el ListView
     * cada vez que sea
     * necesario obtener algun elemento de
     * la lista para mostrarlo al usuario
     *
     * @param position Posicion del item dentro de la lista
     * @param convertView item ya generado y guardado
     * @param parent El grupo completo de la lista
     *
     * @return View
     * */
    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        //vistaResultado será la vista
        //final lista para ser retornada
        View vistaResultado = convertView;
        //TextView dentro del layout a inflar
        TextView tv;
        //ViewHolder utilizado para optimizacion
        ViewHolder holder;

        //si no existe es la primera
        //vez que getView es llamada
        if (vistaResultado == null) {
            //se "infla" el layot "mi_fila2.xml"
            vistaResultado =
                    li.inflate(R.layout.mi_fila2, parent,
                            false);
            //Se hace referencia al
            //textvio dentro de mi_fila2.xml
            tv = (TextView) vistaResultado.findViewById(
                    R.id.mifila_textView);
            // se genera un ViewHolder
            holder = new ViewHolder(tv);
            //se guarda el view holder
            //dentro de la vista resultado
            vistaResultado.setTag(holder);
        } else {
            //en caso de ya existir
            //la vista se obtiene el ViewHolder
            holder = (ViewHolder) vistaResultado.getTag();
        }

        //Se pone como texto
        //el item en la posicion que
        //tendra esta vista en la lista
        holder.tv_programa.setText(programas[position]);

        return vistaResultado;
    }
}
