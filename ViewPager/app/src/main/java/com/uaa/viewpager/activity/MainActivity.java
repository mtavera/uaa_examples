package com.uaa.viewpager.activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.uaa.viewpager.R;
import com.uaa.viewpager.adapter.ColoredPagerAdapter;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.fragmentManager = getSupportFragmentManager();
        this.viewPager = (ViewPager) findViewById(R.id.view_pager);
        ColoredPagerAdapter adapter = new ColoredPagerAdapter(fragmentManager);
        this.viewPager.setAdapter(adapter);

    }
}
