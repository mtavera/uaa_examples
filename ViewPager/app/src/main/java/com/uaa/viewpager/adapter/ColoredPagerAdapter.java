package com.uaa.viewpager.adapter;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.uaa.viewpager.fragment.ColoredFragment;

import java.util.Random;

/***
 * Created by Mario1 on 06/11/16.
 */
public class ColoredPagerAdapter extends FragmentStatePagerAdapter {

    private Random random = new Random();

    public ColoredPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ColoredFragment.getInstance(getColor());
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    private int getColor() {
        return Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }
}
