package com.uaa.menus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * El metodo onCreateOptionsMenu es llamado automaticamente cuando
     * se crea un menu dentro de nuestra actividad
     * */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     *  el evento onOptionsItemSelected es
     *  llamado automaticamente
     *  cuando el usuario da click en
     *  cualquiera de las opciones
     *  desplegadas en la barra de accion
     *  o en un menu para versiones antiguas
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        String seleccion = "";
        switch (item.getItemId()) {
            case R.id.action_settings:
                seleccion = "Settings";
                break;
            case R.id.action_search:
                seleccion = "Search";
                break;
            case R.id.action_miaccion:
                seleccion = "Custom Action";
            default:
                return super.onOptionsItemSelected(item);
        }
        Toast.makeText(this, seleccion, Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }
}
