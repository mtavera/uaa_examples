package com.uaa.animaciones;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button[] botones;
    private TextView textoAnimado;
    private Animation[] anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
        this.initEventos();
    }

    private void init() {
        this.botones = new Button[]{
                (Button) this.findViewById(R.id.bAlpha),
                (Button) this.findViewById(R.id.bScale),
                (Button) this.findViewById(R.id.bRotate),
                (Button) this.findViewById(R.id.bTranslate),
                (Button) this.findViewById(R.id.bSet)
        };

        anim = new Animation[]{
                AnimationUtils.loadAnimation(this,
                        R.anim.alpha_animation),
                AnimationUtils.loadAnimation(this,
                        R.anim.scale_animation),
                AnimationUtils.loadAnimation(this,
                        R.anim.rotate_animation),
                AnimationUtils.loadAnimation(this,
                        R.anim.translate_animation),
                AnimationUtils.loadAnimation(this,
                        R.anim.set_animation)
        };

        textoAnimado = (TextView)
                findViewById(R.id.textoAnimado);

    }

    private void initEventos() {
        View.OnClickListener l = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.bAlpha:
                        textoAnimado.startAnimation(anim[0]);
                        break;
                    case R.id.bScale:
                        textoAnimado.startAnimation(anim[1]);
                        break;
                    case R.id.bRotate:
                        textoAnimado.startAnimation(anim[2]);
                        break;
                    case R.id.bTranslate:
                        textoAnimado.startAnimation(anim[3]);
                        break;
                    case R.id.bSet:
                        textoAnimado.startAnimation(anim[4]);
                }

            }
        };

        for (Button boton : this.botones) {
            boton.setOnClickListener(l);
        }
    }
}
