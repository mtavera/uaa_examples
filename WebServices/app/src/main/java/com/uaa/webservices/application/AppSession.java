package com.uaa.webservices.application;

import android.app.Application;

import com.uaa.webservices.util.VolleyHelper;

/***
 * Created by Mario1 on 19/11/16.
 */
public class AppSession extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        VolleyHelper.initRequestQueue(this);
    }
}
