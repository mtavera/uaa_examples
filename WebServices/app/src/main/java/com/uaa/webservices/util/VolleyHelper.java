package com.uaa.webservices.util;

import android.app.Application;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.uaa.webservices.model.Person;

import org.json.JSONArray;
import org.json.JSONObject;

/***
 * Created by Mario1 on 16/11/16.
 */
public class VolleyHelper {

    private static RequestQueue requestQueue;
    private final static String IP = "192.168.1.79:3000";
    private final static String ENDPOINT = "http://" + IP;

    public final static String PEOPLE_URL = "people/";

    //ideally this should be call during application creation
    public static void initRequestQueue(Application context) {
        requestQueue = Volley.newRequestQueue(context);
    }

    public static void getRequestArray(String route,
                                       Response.Listener<JSONArray> successListener,
                                       Response.ErrorListener errorListener) {

        //create request
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                ENDPOINT + "/" + route,
                null, successListener, errorListener);

        addToQueue(request);
    }

    public static void getRequest(String route,
                                  Response.Listener<JSONObject> successListener,
                                  Response.ErrorListener errorListener) {

        //create request
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                ENDPOINT + "/" + route,
                null, successListener, errorListener);

        addToQueue(request);

    }

    public static void postRequest(String route,
                                   Person person,
                                   Response.Listener<JSONObject> successListener,
                                   Response.ErrorListener errorListener) {
        //create request
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                ENDPOINT + "/" + route,
                person.toJSONObject(),
                successListener,
                errorListener
        );

        addToQueue(jsonObjectRequest);
    }

    public static void putRequest(String route,
                           Person person,
                           Response.Listener<JSONObject> successListener,
                           Response.ErrorListener errorListener) {
        //create request
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.PUT,
                ENDPOINT + "/" + route,
                person.toJSONObject(),
                successListener,
                errorListener
        );

        addToQueue(request);
    }

    public static void deleteRequest(String route,
                                     Response.Listener<JSONObject> successListener,
                                     Response.ErrorListener errorListener) {
        //create request
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.DELETE,
                ENDPOINT + "/" + route,
                null, successListener, errorListener);

        addToQueue(request);
    }

    private static void addToQueue(JsonObjectRequest jsonObjectRequest) {
        if (requestQueue != null)
            requestQueue.add(jsonObjectRequest);
    }

    private static void addToQueue(JsonArrayRequest jsArrayRequest) {
        if (requestQueue != null)
            requestQueue.add(jsArrayRequest);
    }

}
