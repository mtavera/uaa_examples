package com.uaa.webservices.model;

import org.json.JSONException;
import org.json.JSONObject;

/***
 * Created by Mario1 on 19/11/16.
 */
public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private String sex;

    public Person() {
    }

    public Person(JSONObject jsonObject) {
        try {
            this.id = jsonObject.getInt("id");
            this.firstName = jsonObject.getString("first_name");
            this.lastName = jsonObject.getString("last_name");
            this.age = jsonObject.getInt("age");
            this.sex = jsonObject.getString("sex");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Person(int id, String firstName, String lastName, int age, String sex) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", this.id);
            jsonObject.put("first_name", this.firstName);
            jsonObject.put("last_name", this.lastName);
            jsonObject.put("age", this.age);
            jsonObject.put("sex", this.sex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
