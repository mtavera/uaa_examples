package com.uaa.webservices.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.uaa.webservices.R;
import com.uaa.webservices.model.Person;
import com.uaa.webservices.util.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private ProgressDialog dialog;
    private int[] buttonIds = {R.id.btn_get, R.id.btn_post, R.id.btn_put, R.id.btn_del};
    private EditText editId;
    private EditText editFirstName;
    private EditText editLastname;
    private EditText editAge;
    private EditText editSex;
    private TextView txtResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
        this.initListeners();
    }


    private void init() {
        this.editId = (EditText) findViewById(R.id.edt_id);
        this.editFirstName = (EditText) findViewById(R.id.edt_fname);
        this.editLastname = (EditText) findViewById(R.id.edt_lname);
        this.editAge = (EditText) findViewById(R.id.edt_age);
        this.editSex = (EditText) findViewById(R.id.edt_sex);
        this.txtResponse = (TextView) findViewById(R.id.txt_response);
    }

    private void initListeners() {
        View.OnClickListener l = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_get:
                        doGetRequest();
                        break;
                    case R.id.btn_post:
                        doPostRequest();
                        break;
                    case R.id.btn_put:
                        doPutRequest();
                        break;
                    case R.id.btn_del:
                        doDeleteRequest();
                        break;
                }
            }
        };

        for (int btnId : buttonIds) {
            findViewById(btnId).setOnClickListener(l);
        }
    }

    private void doGetRequest() {
        String id = this.editId.getText().toString();
        if (id.isEmpty()) {
            getAllInformation();
        } else {
            getInformationForId(id);
        }
    }

    private void getAllInformation() {
        showLoadingDialog();

        VolleyHelper.getRequestArray(VolleyHelper.PEOPLE_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int arrayLength = response.length();
                        txtResponse.setText("");
                        clearEditText();
                        for (int i = 0; i < arrayLength; i++) {
                            try {
                                Person person = new Person(response.getJSONObject(i));
                                txtResponse.append(person.toString() + '\n');
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        hideLoadingDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        if (error.networkResponse.statusCode == 404) {
                            txtResponse.setText("Not found :(");
                            clearEditText();
                        }
                        hideLoadingDialog();
                    }
                });
    }


    private void getInformationForId(String id) {
        showLoadingDialog();

        VolleyHelper.getRequest(VolleyHelper.PEOPLE_URL + id,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Person person = new Person(response);
                        txtResponse.setText(person.toString());
                        setEditTextInformation(person);
                        hideLoadingDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, error.toString());
                        if (error.networkResponse.statusCode == 404) {
                            txtResponse.setText("Not found :(");
                            clearEditText();
                        }
                        hideLoadingDialog();
                    }
                });

    }

    private void doPostRequest() {
        showLoadingDialog();

        Person person = getPersonFromEditTextValues();

        VolleyHelper.postRequest(
                VolleyHelper.PEOPLE_URL,
                person,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoadingDialog();
                        txtResponse.setText("New person added!");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideLoadingDialog();
                        Log.e(TAG, error.toString());
                        txtResponse.setText("Something went wrong :(");
                    }
                });
    }

    private void doPutRequest() {
        showLoadingDialog();
        String id = this.editId.getText().toString();
        if (id.isEmpty()) {
            this.txtResponse.setText("And id must be defined to do a PUT call");
            hideLoadingDialog();
        } else {
            Person person = getPersonFromEditTextValues();
            VolleyHelper.putRequest(
                    VolleyHelper.PEOPLE_URL + id,
                    person,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            hideLoadingDialog();
                            txtResponse.setText("Person data updated!");
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideLoadingDialog();
                            Log.e(TAG, error.toString());
                            txtResponse.setText("Something went wrong :(");
                        }
                    }
            );
        }

    }

    private void doDeleteRequest() {
        showLoadingDialog();
        String id = this.editId.getText().toString();
        if (id.isEmpty()) {
            this.txtResponse.setText("And id must be defined to do a DELETE call");
            hideLoadingDialog();
        } else {
            VolleyHelper.deleteRequest(
                    VolleyHelper.PEOPLE_URL + id,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            txtResponse.setText("Person deleted successfully");
                            hideLoadingDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideLoadingDialog();
                            Log.e(TAG, error.toString());
                            txtResponse.setText("Something went wrong :(");
                        }
                    }
            );
        }
    }

    private void setEditTextInformation(Person person) {
        this.editId.setText(String.valueOf(person.getId()));
        this.editFirstName.setText(String.valueOf(person.getFirstName()));
        this.editLastname.setText(String.valueOf(person.getLastName()));
        this.editAge.setText(String.valueOf(person.getAge()));
        this.editSex.setText(String.valueOf(person.getSex()));
    }

    private void clearEditText() {
        this.editFirstName.setText("");
        this.editLastname.setText("");
        this.editAge.setText("");
        this.editSex.setText("");
    }

    private Person getPersonFromEditTextValues() {
        //get data from editText
        int id = Integer.parseInt(this.editId.getText().toString());
        String firstName = this.editFirstName.getText().toString();
        String lastName = this.editLastname.getText().toString();
        int age = Integer.parseInt(this.editAge.getText().toString());
        String sex = this.editSex.getText().toString();

        //create person model
        Person person = new Person(id, firstName, lastName, age, sex);
        return person;
    }

    private void showLoadingDialog() {
        this.dialog = ProgressDialog.show(this, "",
                "Loading. Please wait...", true);
        this.dialog.show();
    }

    private void hideLoadingDialog() {
        if (dialog != null) {
            this.dialog.hide();
            this.dialog = null;
        }
    }
}
