package com.uaa.fragments;

import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Obtenemos el manejador de fragmentos
        FragmentManager fragmentManager = getSupportFragmentManager();

        //Para poder hacer cambios a los fragmentos
        //es necesario iniciar transacciones
        //(Añadir, remover y reemplazar fragmentos)
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();

        //obtenemos la configuracion del sistema
        Configuration configInfo =
                getResources().getConfiguration();

        //si el dispositivo se encuentra en modo landscape
        //se mostrá el fragmento "FragmentLandscape"
        //wn caso contrario se mostrara "FragmentPortrait"
        if (configInfo.orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {
            FragmentLandscape fragmentLandscape = new FragmentLandscape();
            fragmentTransaction.replace(R.id.frame_container, fragmentLandscape);
        } else {
            FragmentPortrait fragmentPortrait = new FragmentPortrait();
            fragmentTransaction.replace(R.id.frame_container, fragmentPortrait);
        }

        //siempre es necesario comitear la transaccion
        fragmentTransaction.commit();
    }
}
