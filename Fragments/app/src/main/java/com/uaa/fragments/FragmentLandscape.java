package com.uaa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/***
 * Created by Mario1 on 03/10/15.
 */
public class FragmentLandscape extends Fragment {

    //los fragmentos tienen su propio ciclo de vida
    //al igual que las actividades

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                         ViewGroup container,
                         Bundle savedInstanceState) {

        return inflater.inflate(R.layout.landscape_fragment,
                        container, false);
    }
}
