package com.uaa.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/***
 * Created by Mario1 on 03/10/15.
 */
public class FragmentPortrait extends Fragment {

    //los fragmentos tienen su propio cilco de vida
    //al igual que las actividades

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
          ViewGroup container, Bundle savedInstanceState) {
        //se inflara el layout "portrait fragment"
        //el fragmento tendra como UI este layout
        return inflater
                .inflate(R.layout.portrait_fragment,
                        container, false);
    }
}
