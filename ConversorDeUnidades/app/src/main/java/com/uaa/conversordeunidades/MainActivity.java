package com.uaa.conversordeunidades;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    TextView cantidad;
    Spinner type;
    Spinner conversion;
    EditText num;
    Button calcular;
    ArrayAdapter<CharSequence> adapterConv[];
    int sel;
    int selConversion;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);cantidad = (TextView)this.findViewById(R.id.cant);
        num = (EditText)this.findViewById(R.id.Num);
        calcular = (Button)this.findViewById(R.id.Calcular);
        //Declaracion de los spinners
        type = (Spinner)this.findViewById(R.id.tipo);
        conversion = (Spinner)this.findViewById(R.id.conversion);
        //Creacion de adaptadores
        ArrayAdapter<CharSequence> adapterType = ArrayAdapter.createFromResource(this, R.array.tipos, android.R.layout.simple_spinner_item);
        //Se pone el adaptador de longitud ya que es el primero en ponerse siempre
        adapterConv = new ArrayAdapter[6];
        adapterConv[0] = ArrayAdapter.createFromResource(this, R.array.longitud, android.R.layout.simple_spinner_item);
        adapterConv[1] = ArrayAdapter.createFromResource(this, R.array.temp, android.R.layout.simple_spinner_item);
        adapterConv[2] = ArrayAdapter.createFromResource(this, R.array.vol, android.R.layout.simple_spinner_item);
        adapterConv[3] = ArrayAdapter.createFromResource(this, R.array.peso, android.R.layout.simple_spinner_item);
        adapterConv[4] = ArrayAdapter.createFromResource(this, R.array.tiempo, android.R.layout.simple_spinner_item);
        adapterConv[5] = ArrayAdapter.createFromResource(this, R.array.vel, android.R.layout.simple_spinner_item);
        type.setAdapter(adapterType);
        conversion.setAdapter(adapterConv[0]);
        //Eventos de los spinners
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View arg1,int pos, long arg3) {
                switch (pos){
                    case 0:  conversion.setAdapter(adapterConv[0]); sel=0 ;break;
                    case 1: conversion.setAdapter(adapterConv[1]); sel=1; break;
                    case 2: conversion.setAdapter(adapterConv[2]); sel=2; break;
                    case 3: conversion.setAdapter(adapterConv[3]); sel=3; break;
                    case 4: conversion.setAdapter(adapterConv[4]); sel=4; break;
                    case 5: conversion.setAdapter(adapterConv[5]); sel=5; break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
        conversion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View arg1, int pos, long arg3) {
                selConversion = pos;
                cantidad.setText(String.valueOf(0.0));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {}
        });
        //EventoBoton
        calcular.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                try{
                    switch(sel){
                        case 0:
                            switch(selConversion){
                                case	0: cantidad.setText( (Double.parseDouble(num.getText().toString())*0.62 )+" mi"); break;
                                case	1: cantidad.setText( (Double.parseDouble(num.getText().toString())*0.39 )+" in"); break;
                                case	2: cantidad.setText( (Double.parseDouble(num.getText().toString())/0.62 )+" Km"); break;
                                case	3: cantidad.setText( (Double.parseDouble(num.getText().toString())/0.39 )+" Cm"); break;
                            }
                            break;
                        case 1:
                            switch(selConversion){
                                case	0: cantidad.setText( ((Double.parseDouble(num.getText().toString())-32)*5/9 )+" C"); break;
                                case	1: cantidad.setText( (Double.parseDouble(num.getText().toString())*9/5+32 )+" F"); break;
                                case	2: cantidad.setText( (Double.parseDouble(num.getText().toString())-273.15 )+" K"); break;
                            }
                            break;
                        case 2:
                            switch(selConversion){
                                case	0: cantidad.setText( (Double.parseDouble(num.getText().toString())/3.78541178 )+" Gal"); break;
                                case	1: cantidad.setText( (Double.parseDouble(num.getText().toString())*3.78541178 )+" L"); break;
                                case	2: cantidad.setText( (Double.parseDouble(num.getText().toString())* 29.57 )+" mL"); break;
                                case	3: cantidad.setText( (Double.parseDouble(num.getText().toString())/29.57 )+" onz"); break;
                            }
                            break;
                        case 3:
                            switch(selConversion){
                                case	0: cantidad.setText( (Double.parseDouble(num.getText().toString())*2.20462262 )+" Lb"); break;
                                case	1: cantidad.setText( (Double.parseDouble(num.getText().toString())/2.20462262 )+" Kg"); break;
                                case	2: cantidad.setText( (Double.parseDouble(num.getText().toString())*0.0352739619 )+" onz"); break;
                                case	3: cantidad.setText( (Double.parseDouble(num.getText().toString())/0.0352739619 )+" g"); break;
                            }
                            break;
                        case 4:
                            switch(selConversion){
                                case	0: cantidad.setText( (Double.parseDouble(num.getText().toString())*60 )+" min"); break;
                                case	1: cantidad.setText( (Double.parseDouble(num.getText().toString())/60 )+" hr"); break;
                                case	2: cantidad.setText( (Double.parseDouble(num.getText().toString())*3600 )+" seg"); break;
                                case	3: cantidad.setText( (Double.parseDouble(num.getText().toString())/3600 )+" hr"); break;
                            }
                            break;
                        case 5:
                            switch(selConversion){
                                case	0: cantidad.setText( (Double.parseDouble(num.getText().toString())*0.27 )+" m/s"); break;
                                case	1: cantidad.setText( (Double.parseDouble(num.getText().toString())/0.27 )+" km/hr"); break;
                                case	2: cantidad.setText( (Double.parseDouble(num.getText().toString())*0.62 )+" mi/hr"); break;
                                case	3: cantidad.setText( (Double.parseDouble(num.getText().toString())/0.62 )+" km/hr"); break;
                            }
                            break;
                    }
                } catch(NumberFormatException ignore){

                }
            }
        });

    }


}
