package com.uaa.maps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends AppCompatActivity {

    private GoogleMap mMap;
    private final int LOCATION_PERMISSION_CODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.initMap();
    }

    private void initMap() {
        //Obtener fragmentmanager
        FragmentManager fragmentManager = getSupportFragmentManager();

        //Buscar mapFragment
        SupportMapFragment supportMapFragment = (SupportMapFragment)
                fragmentManager.findFragmentById(R.id.map);

        //Obtener objeto google maps, esto es asyncrono
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                //Mostramos contorles de zoom e identificacion de
                //posicion actual en el mapa
                mMap.getUiSettings().setZoomControlsEnabled(true);

                if (checkForLocationPermission()) {
                    mMap.setMyLocationEnabled(true);
                    mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                        @Override
                        public void onMyLocationChange(Location location) {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
                            mMap.setOnMyLocationChangeListener(null);
                        }
                    });
                } else {
                    requestLocationPermission();
                }

                //descripcion del contenido de la vista
                mMap.setContentDescription("Edificios de la UAA");

                //agregar marker
                addMarker();

            }
        });

    }

    private void addMarker() {
        mMap.addMarker(new MarkerOptions()
                //lat, long
                .position(new LatLng(21.9135183, -102.3157933))
                .title("Edificio 55")
                .snippet("Estamos cerca de aqui")
                .draggable(false));
    }

    private boolean checkForLocationPermission() {

        boolean isFineLocationGranted =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED;

        boolean isCoarseLocationGranted = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        return isFineLocationGranted && isCoarseLocationGranted;
    }

    private void requestLocationPermission() {
        String[] permissionToRequest = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        ActivityCompat.requestPermissions(this, permissionToRequest, LOCATION_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (checkForLocationPermission()) {
                mMap.setMyLocationEnabled(true);
            }
        }
    }
}
