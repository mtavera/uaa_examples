package com.uaa.controlpersonalizado1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

//Para generar un control a partir de uno
//ya existente se exitende el control del cual
//estara basado el nuestro
public class EditTextPersonalizado extends EditText {

    private Paint p1;
    private Paint p2;

    private float densidad;

    // Constructures utilizados por la clase EditText
    public EditTextPersonalizado(Context context) {
        super(context);
        init();
    }

    public EditTextPersonalizado(Context context,
                                 AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public EditTextPersonalizado(Context context,
                                 AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    // Aqui inicializamos configuracion de
    // nuestros pinceles y densidad de la pantalla
    private void init() {
        this.p1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.p1.setColor(Color.BLACK);
        this.p1.setStyle(Paint.Style.FILL);

        this.p2 = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.p2.setColor(Color.WHITE);
        // this.p2.setTextSize(20);

        this.densidad = getResources()
                .getDisplayMetrics().density;
    }

    //Este metodo se manda llamar automaticamente para pintar
    //el control en la pantalla
    //recibe de paramatros un objeto canvas
    @Override
    public void onDraw(Canvas canvas) {

        // Llamamos al método de la superclase
        //esto pintara el control de manera automatica
        //como un EditText
        super.onDraw(canvas);

        // Dibujamos el fondo negro del contador dibujando
        // un rectangulo
        // utilizando el metodo drawRect que recibe de parametros las
        // coordenadas relativas al tamaño de nuestro control
        // (float left, float top, float right, float bottom,
        // Paint paint)
        float izquierda = this.getWidth() - 30 * this.densidad,
                arriba = 5 * this.densidad, derecha = this
                .getWidth() - 5 * this.densidad,
                abajo = 20 * this.densidad;
        canvas.drawRect(izquierda, arriba, derecha, abajo, p1);

        //Como dibujaremos texto debemos especificar el tamaño
        // de este
        p2.setTextSize(10 * densidad);

        // Dibujamos el numero de caracteres sobre el contador
        // utilizando el metodo drawText, tambien utiliza
        // las coordenadas relativas de nuestro
        // control, note que se multiplica por la densidad
        // de pantalla ya que estos valores estan dados en pixeles
        // (String text, float x, float y, Paint paint)
        int tamanoTexto = this.getText().toString().length();
        float x = this.getWidth() - 28 * this.densidad,
                y = 17 * this.densidad;
        canvas.drawText(String.valueOf(tamanoTexto), x, y, p2);
    }

}

