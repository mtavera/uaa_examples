package com.uaa.notificaciones;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

public class MiDialogoConfimacion extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder =
				new AlertDialog.Builder(getActivity());

		builder.setMessage("Confirma la accion seleccionada?")
				.setTitle("Confirmacion");

		builder.setPositiveButton("Aceptar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
											int id) {
						Log.d("MiDialogoConfirmacion","Aceptar");
								dialog.cancel();
					}
				});

		builder.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
												int id) {
								Log.d("MiDialogoConfirmacion","Cancelar");
								dialog.cancel();
							}
						});

		return builder.create();
	}
}
