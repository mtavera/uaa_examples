package com.uaa.notificaciones;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /* id utilizado para notificaciones
   en la Barra de Estado */
    private static final int NOTIF_ALERTA_ID = 0;
    /* array donde guardaremos
    los botones de nuestro layout*/
    private Button[] misBotones;
    /*
       builder para generar notificacion
       en la barra de accion
       La clase NotificationCompat.Builder
       nos permite crear notificaciones
       compatibles con versiones de Android antiguas
    */
    private NotificationCompat.Builder mBuilder;
    /* Servicio de notificaciones en Android */
    private NotificationManager mNotificationManager;
    /* Administrador de fragmentos */
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializaElementos();
        setBotonesClickListener();
    }

    /**
     * Metodo utilizado para inicializar
     * los elementos necesarios de nuestra aplicacion
     */
    private void inicializaElementos() {
        /* Obtenemos referencia a nuestros botones */
        misBotones = new Button[] {
                (Button) findViewById(R.id.mToast),
                (Button) findViewById(R.id.mDialogo),
          (Button) findViewById(R.id.mDialogoConfirm),
          (Button) findViewById(R.id.mDialogoSel),
          (Button) findViewById(R.id.mDialogoSel2),
          (Button) findViewById(R.id.mDialogoSel3),
                (Button) findViewById(R.id.mEstado),
        };

        /*
          Obtenemos el servcio de notificaciones
          a travez del metodo getSystemService
        */
        mNotificationManager =
                (NotificationManager) getSystemService
                        (Context.NOTIFICATION_SERVICE);

        Drawable iconDrawable =
                ContextCompat.getDrawable(this,
                        R.mipmap.ic_launcher);
        Bitmap icon = ((BitmapDrawable) iconDrawable)
                .getBitmap();

        mBuilder =
                new NotificationCompat.
                        Builder(MainActivity.this)  //mandamos MainActivity como contexto
                        .setSmallIcon(
                        android.R.drawable.stat_sys_warning)
                        .setLargeIcon(icon)
                        .setContentTitle("Titulo de Alerta")
                .setContentText("Ejemplo de notificación " +
                                "en barra de estado.")
                        .setContentInfo("content info")
                        .setTicker("ticker!");

        // Obtenemos el fragmentManager
        // que sera utilizado en los dialogos
        fragmentManager = getSupportFragmentManager();

    }

    private void setBotonesClickListener() {
        View.OnClickListener l = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.mToast:
                        Toast.makeText(MainActivity.this,
                                "Mostrando Toast",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.mEstado:
                        mNotificationManager
                                .notify(NOTIF_ALERTA_ID,
                                        mBuilder.build());
                        break;
                    case R.id.mDialogo:
                        MiDialogo dialogo = new MiDialogo();
                        dialogo.show(fragmentManager, "dialogo");
                        break;
                    case R.id.mDialogoConfirm:
                        MiDialogoConfimacion mdc =
                                new MiDialogoConfimacion();
                        mdc.show(fragmentManager, "mdc");
                        break;
                    case R.id.mDialogoSel:
                        MiDialogoSeleccion mds =
                                new MiDialogoSeleccion();
                        mds.show(fragmentManager, "mds");
                        break;
                    case R.id.mDialogoSel2:
                        MiDialogoSeleccion2 mds2 =
                                new MiDialogoSeleccion2();
                        mds2.show(fragmentManager, "mds2");
                        break;
                    case R.id.mDialogoSel3:
                        MiDialogoSeleccion3 mds3 =
                                new MiDialogoSeleccion3();
                        mds3.show(fragmentManager, "mds3");
                        break;

                }
            }
        };
        for (Button boton : misBotones) {
            boton.setOnClickListener(l);
        }
    }
}
