package com.uaa.notificaciones;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

public class MiDialogo extends DialogFragment {
	@Override
	public Dialog onCreateDialog
	(Bundle savedInstanceState) {

		AlertDialog.Builder builder = 
				new AlertDialog.Builder(getActivity());

		builder.setMessage("Esto es un Dialogo")
				.setTitle("Titulo del dialogo");

		builder.setPositiveButton("OK",
		 new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface
									dialog, int id) {
						//cerramos el dialogo
						Log.d("MiDialogo","Click OK");
						dialog.cancel();
					}
				});

		return builder.create();
	}
}
