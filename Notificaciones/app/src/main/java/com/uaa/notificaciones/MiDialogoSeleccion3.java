package com.uaa.notificaciones;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

public class MiDialogoSeleccion3 extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final String[] items = {"Seleccion 1",
                "Seleccion 2", "Seleccion 3"};

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());

        builder.setTitle("Seleccion")
                .setMultiChoiceItems(items,
                        new boolean[]{false, false, false},
          new DialogInterface.OnMultiChoiceClickListener() {
              public void onClick(DialogInterface dialog,
                                  int item,
                                  boolean isChecked) {
                    Log.i("Dialogos", "Opcion elegida: "
                            + items[item]);
                            }
                    });

        return builder.create();
    }
}
