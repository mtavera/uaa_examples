package com.uaa.notificaciones;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

public class MiDialogoSeleccion extends DialogFragment {
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final String[] items = { "Seleccion 1",
				"Seleccion 2", "Seleccion 3" };

		AlertDialog.Builder builder =
				new AlertDialog.Builder(getActivity());

		builder.setTitle("Seleccion");
		builder.setItems(items,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
										int item) {
						Toast.makeText(getActivity(),
								"Opcion elegida: " + items[item],
								Toast.LENGTH_SHORT)
						  .show();
						Log.d("Dialogos", "Opcion elegida: "
								+ items[item]);
					}
				});
		
		
		return builder.create();
	}
}