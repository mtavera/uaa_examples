package com.uaa.layoutsalternos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int buttonIds[] = new int[]{
            R.id.button1, R.id.button2, R.id.button3,
            R.id.button4, R.id.button5, R.id.button6,
            R.id.button7, R.id.button8, R.id.button9,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View.OnClickListener onClickListener =
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                Toast.makeText(MainActivity.this,
                        button.getText(),
                        Toast.LENGTH_SHORT).show();
            }
        };

        for (int buttonId : buttonIds) {
            findViewById(buttonId).setOnClickListener(
                    onClickListener);
        }
    }
}
