package com.uaa.multiplefragmentlayouts.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.uaa.multiplefragmentlayouts.fragment.SuperHeroDetailsFragment;

/**
 * Created by Mario1 on 04/10/15.
 */
public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //si estamos en landscape, cerramos actividad
        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
        } else {
            //Creamos nueva instancia de SuperHeroDetailsFragment
            //y mandamos como argumetnos, los extras obtenidos del intent
            SuperHeroDetailsFragment superHeroDetailsFragment = new SuperHeroDetailsFragment();
            superHeroDetailsFragment.setArguments(getIntent().getExtras());
            //iniciamos transaccion de fragmentos
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(android.R.id.content, superHeroDetailsFragment);
            fragmentTransaction.commit();
        }
    }
}
