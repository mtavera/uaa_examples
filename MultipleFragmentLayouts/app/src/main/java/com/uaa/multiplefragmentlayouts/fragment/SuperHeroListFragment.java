package com.uaa.multiplefragmentlayouts.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.uaa.multiplefragmentlayouts.R;
import com.uaa.multiplefragmentlayouts.activity.DetailsActivity;
import com.uaa.multiplefragmentlayouts.model.SuperHeroInfo;

/***
 * Created by Mario1 on 30/10/16.
 */

public class SuperHeroListFragment extends Fragment {

    private int mCurCheckPosition;
    private boolean mDualPane;

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_super_hero_list, container, false);
        this.listView = (ListView) rootView.findViewById(R.id.list_view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, SuperHeroInfo.NAMES);

        this.listView.setAdapter(adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetails(position);
            }
        });


        return rootView;
    }

    private void showDetails(int index) {

        this.mCurCheckPosition = index;

        //si DetailsFrame es nulo, o no es visible, quiere decir
        //que nos encontramos en landscape
        View detailsFrame = getActivity().findViewById(R.id.details);
        mDualPane = detailsFrame != null
                && detailsFrame.getVisibility() == View.VISIBLE;
        if (mDualPane) {
            listView.setItemChecked(index, true);

            SuperHeroDetailsFragment superHeroDetailsFragment =
                    (SuperHeroDetailsFragment) getFragmentManager()
                            .findFragmentById(R.id.details);

            if (superHeroDetailsFragment == null
                    || superHeroDetailsFragment.getIndex() != index) {

                superHeroDetailsFragment = SuperHeroDetailsFragment.newInstance(index);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.details, superHeroDetailsFragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        } else {
            Intent intent = new Intent(getActivity(), DetailsActivity.class);
            intent.putExtra("index", mCurCheckPosition);
            startActivity(intent);
        }
    }
}
