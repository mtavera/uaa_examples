package com.uaa.multiplefragmentlayouts.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uaa.multiplefragmentlayouts.R;
import com.uaa.multiplefragmentlayouts.model.SuperHeroInfo;

/***
 * Created by Mario1 on 03/10/15.
 */
public class SuperHeroDetailsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sueper_hero_details, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.text_view);
        textView.setText(SuperHeroInfo.HISTORY[getIndex()]);
        return rootView;
    }


    public int getIndex() {
        return getArguments().getInt("index");
    }

    public static SuperHeroDetailsFragment newInstance(int index) {
        SuperHeroDetailsFragment f = new SuperHeroDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        return f;
    }
}
