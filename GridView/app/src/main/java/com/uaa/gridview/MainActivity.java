package com.uaa.gridview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Constante donde definimos el tamaño del array que
    //se mandara al adapter
    private final int TAMANO_DATOS = 50;

    // array que se mandara al adapter
    private String[] datos = new String[TAMANO_DATOS];

    // contexto de nuestra aplicacion
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtener contexto de la aplicacion e igualarlo
        //a nuestra variable context
        this.context = MainActivity.this;

        // Generando array a utilizar en el adaptador
        for (int i = 1; i <= this.TAMANO_DATOS; i++) {
            datos[i - 1] = "Dato " + i;
        }

        //adaptador a usar en nuestro GridView
        ArrayAdapter<String> adaptador =
                new ArrayAdapter<>(context,
                        android.R.layout.simple_list_item_1,
                        datos);

        AdaptadorPropio adaptadorPropio =
                new AdaptadorPropio(context, 0, datos);

        // "Buscamos" nuestro gridView con id = GridOpciones en
        // nuestro layout
        GridView grdOpciones = (GridView) findViewById(
                R.id.gridOpciones);

        //Ponemos el adaptador generado arriba en nuesro gridView
        //grdOpciones.setAdapter(adaptador);
        grdOpciones.setAdapter(adaptadorPropio);

        // registramos escuchadores en nuestro gridView,
        // cada vez se se de "click"
        // a uno de nuestros items
        grdOpciones
                .setOnItemClickListener(
                        new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> parent,
                                                    android.view.View v, int position, long id) {

                                Toast.makeText(MainActivity.this,
                                        "Opcion seleccionada: " + datos[position],
                                        Toast.LENGTH_SHORT).show();
                            }

                        });
    }
}
