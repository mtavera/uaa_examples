package com.uaa.gridview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdaptadorPropio
        extends ArrayAdapter<String> {

    LayoutInflater li;
    String[] textos;

    static class ViewHolder {
        public TextView contacto;
        public ImageView imagen;

        public ViewHolder(ImageView imagen,
                          TextView contacto) {
            super();
            this.contacto = contacto;
            this.imagen = imagen;
        }

    }

    public AdaptadorPropio(Context context,
                           int resource,
                           String[] objects) {
        super(context, resource, objects);
        this.textos = objects;
        this.li = (LayoutInflater) context
             .getSystemService(
                Context
                  .LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position,
                        View convertView,
                        ViewGroup parent) {

        View vistaResultado = convertView;
        ImageView iv;
        TextView tv;
        ViewHolder holder;

        if (vistaResultado == null) {
            vistaResultado = li
               .inflate(R.layout.grid_individual,
                       parent, false);
            iv = (ImageView)
                    vistaResultado
                            .findViewById(
                                    R.id.grid_imagen);
            tv = (TextView)
                    vistaResultado
                            .findViewById(
                                    R.id.grid_texto);
            holder = new ViewHolder(iv, tv);
            vistaResultado.setTag(holder);
        } else {
            holder = (ViewHolder) vistaResultado.getTag();
        }

        holder
             .imagen
                .setImageResource(R.mipmap.ic_launcher);
        holder
             .contacto
                .setText(textos[position]);

        return vistaResultado;
    }
}
