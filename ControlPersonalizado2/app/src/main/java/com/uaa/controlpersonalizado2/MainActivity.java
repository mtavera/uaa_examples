package com.uaa.controlpersonalizado2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private ControlLogin ctlLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ctlLogin = (ControlLogin)
                findViewById(R.id.ctrl_login);

        ctlLogin.setOnLoginListener(new ControlLogin.OnLoginListener() {
            @Override
            public void onLogin(String usuario,
                                String password) {
                // Validamos el usuario y la contraseña
                if (usuario.equals("admin") &&
                        password.equals("admin")) {
                    ctlLogin.setMensaje("Login correcto!",
                            Color.BLUE);
                } else {
                    ctlLogin.setMensaje("Vuelva a intentarlo.",
                            Color.RED);
                }
            }
        });
    }
}
