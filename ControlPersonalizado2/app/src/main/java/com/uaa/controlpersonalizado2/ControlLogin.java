package com.uaa.controlpersonalizado2;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * El layout utilizado en control_login tiene como elemento padre un
 * LinearLayout por lo que extenderemos de este objeto
 * */
public class ControlLogin extends LinearLayout {

    public interface OnLoginListener{
        void onLogin(String usr, String psw);
    }

    private EditText txtUsuario;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView lblMensaje;
    private OnLoginListener listener;

    /*Sobre escritura de constructores de LinearLayout*/
    public ControlLogin(Context context) {
        super(context);
        this.inicializar();
    }

    public ControlLogin(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.inicializar();
    }

    /*
     * Aqui inicializaremos
     * todo lo necesario para
     * que funcione nuestro control
     * */
    private void inicializar() {
        // Utilizamos el layout 'control_login.xml'
        // como interfaz del control
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext()
                .getSystemService(
                        infService);
        li.inflate(R.layout.control_login, this, true);

        // Obtenemos las referencias a los
        // distintos views dentro de este Layout
        txtUsuario = (EditText) findViewById(R.id.TxtUsuario);
        txtPassword = (EditText) findViewById(R.id.TxtPassword);
        btnLogin = (Button) findViewById(R.id.BtnAceptar);
        lblMensaje = (TextView) findViewById(R.id.LblMensaje);

        // Registramos los eventos
        // necesarios dentro de nuestro control
        this.asignarEventos();
    }

    private void asignarEventos() {
        //Cada vez que el usuario de click al boton de login,
        //Se disparará nuestro evento personalizado "onLogin"
        btnLogin.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // Al dar click en el boton se dispara
                // nuestro evento Onlogin,
                // utilizado de parametros el
                // Usuario y la contraseña que
                // escribio el usuario
                listener.onLogin(txtUsuario.getText().toString(),
                        txtPassword.getText().toString());
            }

        });
    }

    /**
     * Metodo utilizado para Mostrar un
     * mensaje al usuario dentro de nuestro control
     *
     * @param msg - String
     * @param color - int
     *
     * */
    public void setMensaje(String msg, int color) {
        lblMensaje.setText(msg);
        lblMensaje.setTextColor(color);
    }

    /**
     * Metodo utilizado para registrar un evento "onLogin" dentro de nuestro
     * control
     *
     * @param l - OnLoginListener
     * */
    public void setOnLoginListener(OnLoginListener l) {
        listener = l;
    }

}
