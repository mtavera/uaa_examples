package com.uaa.fragmenttransitions.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uaa.fragmenttransitions.R;

/***
 * Created by Mario1 on 06/11/16.
 */
public class ColoredFragment extends Fragment {
    public static final String BACKGROUND_COLOR = "BACKGROUND_COLOR";
    public static int instanceCounter;
    private static final String TAG = "ColoredFragment";

    public ColoredFragment() {
        instanceCounter++;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        //obtener color argumento
        int color = getArguments().getInt(BACKGROUND_COLOR);

        //obtener root view inflando fragment_colored.xml
        View rootView = inflater.inflate(R.layout.fragment_colored, container, false);
        rootView.setBackgroundColor(color);

        return rootView;
    }

    public String getTagName() {
        return TAG + instanceCounter;
    }
}
