package com.uaa.fragmenttransitions.activity;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.uaa.fragmenttransitions.R;
import com.uaa.fragmenttransitions.fragment.ColoredFragment;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button btnPrevFragment;
    private Button btnNextFragment;

    private FragmentManager fragmentManager;

    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.init();
    }

    private void init() {
        this.fragmentManager = getSupportFragmentManager();
        this.initViews();
        this.initListeners();
    }

    private void initViews() {
        this.btnNextFragment = (Button) findViewById(R.id.btn_next_fragment);
        this.btnPrevFragment = (Button) findViewById(R.id.btn_prev_fragment);
    }


    private void initListeners() {
        View.OnClickListener buttonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == btnNextFragment) {
                    loadNextFragment();
                } else {
                    loadPrevFragment();
                }
            }
        };
        this.btnNextFragment.setOnClickListener(buttonClickListener);
        this.btnPrevFragment.setOnClickListener(buttonClickListener);
    }

    private void loadNextFragment() {
        ColoredFragment fragment = new ColoredFragment();
        fragment.setArguments(getFragmentArguments());

        FragmentTransaction transaction = this.fragmentManager.beginTransaction();

        /*transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                R.anim.enter_from_left, R.anim.exit_to_right);*/

        /*transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                R.anim.enter_from_left, R.anim.exit_to_right);*/

        /*transaction.setCustomAnimations(R.anim.enter_from_up, R.anim.exit_to_below,
                R.anim.enter_from_up, R.anim.exit_to_below);*/

        /*transaction.setCustomAnimations(R.anim.enter_from_below, R.anim.exit_to_up,
                R.anim.enter_from_below, R.anim.exit_to_up);*/

        transaction.addToBackStack(fragment.getTagName());
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    private void loadPrevFragment() {
        fragmentManager.popBackStackImmediate();
    }

    private Bundle getFragmentArguments() {
        Bundle arguments = new Bundle();
        arguments.putInt(ColoredFragment.BACKGROUND_COLOR, getColor());
        return arguments;
    }

    private int getColor() {
        return Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }


    @Override
    public void onBackPressed() {
        if (!fragmentManager.popBackStackImmediate())
            super.onBackPressed();
    }
}
