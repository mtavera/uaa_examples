package com.uaa.databases.activity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.uaa.databases.util.DBHelper;
import com.uaa.databases.R;

public class MainActivity extends AppCompatActivity {
    private EditText txtId;
    private EditText txtNombre;
    private TextView txtResultado;
    private SQLiteDatabase db;
    private Button[] botones;
    private ContentValues nuevoRegistro;
    private ContentValues valores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.inicializaElementos();
        this.abrirBase();
        this.inicializarListeners();
    }

    private void inicializaElementos() {
        // Obtenemos las referencias a los controles
        txtId = (EditText) findViewById(R.id.txtReg);
        txtNombre = (EditText) findViewById(R.id.txtVal);
        txtResultado = (TextView)
                findViewById(R.id.txtResultado);

        botones = new Button[]{(Button) findViewById(R.id.btnInsertar),
                (Button) findViewById(R.id.btnActualizar),
                (Button) findViewById(R.id.btnEliminar),
                (Button) findViewById(R.id.btnConsultar)
        };
    }

    private void abrirBase() {
        // Abrimos la base de datos 'usuarios.db' en modo escritura
        DBHelper usdbh = new DBHelper(this,
                "usuarios.db", null, 1);

        db = usdbh.getWritableDatabase();
    }

    private void inicializarListeners() {
        View.OnClickListener l = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btnInsertar:
                        insertarBD();
                        break;
                    case R.id.btnActualizar:
                        actualizarBD();
                        break;
                    case R.id.btnEliminar:
                        eleminarBD();
                        break;
                    case R.id.btnConsultar:
                        consultarBD();
                        break;
                }
            }

        };

        for (Button botone : botones) {
            botone.setOnClickListener(l);
        }
    }

    private void insertarBD() {
        // Recuperamos los valores de los campos de texto
        String id = txtId.getText().toString();
        String nom = txtNombre.getText().toString();

        //Alternativa 1: método sqlExec()
        String sql = "INSERT INTO " +
                "Usuarios (id,nombre) VALUES ('"
                + id + "','" + nom + "') ";
        db.execSQL(sql);

        // Alternativa 2: método insert()
        /*nuevoRegistro = null;
        nuevoRegistro = new ContentValues();
        nuevoRegistro.put("id", id);
        nuevoRegistro.put("nombre", nom);
        db.insert("Usuarios", null, nuevoRegistro);*/
    }

    private void actualizarBD() {
        //Recuperamos los valores de los campos de texto
        String id = txtId.getText().toString();
        String nom = txtNombre.getText().toString();

        //Alternativa 1: método sqlExec()
        String sql = "UPDATE Usuarios SET nombre='"
                + nom + "' WHERE id=" + id;
        db.execSQL(sql);

        //Alternativa 2: método update()
        /*valores = null;
        valores = new ContentValues();
        valores.put("nombre", nom);
        db.update("Usuarios", valores, "id=" + id, null);*/
    }

    private void eleminarBD() {
        //Recuperamos los valores de los campos de texto
        String id = txtId.getText().toString();

        //Alternativa 1: método sqlExec()
        String sql = "DELETE FROM Usuarios WHERE id=" + id;
        db.execSQL(sql);

        //Alternativa 2: método delete()
        //db.delete("Usuarios", "id=" + id, null);
    }

    private void consultarBD() {

        //Alternativa 1: método rawQuery()
        Cursor c = db.rawQuery("SELECT id, nombre FROM Usuarios", null);

        //Alternativa 2: método query()
        //String[] campos = new String[] {"id", "nombre"};
        //Cursor c = db.query("Usuarios", campos, null, null, null, null, null);

        //Recorremos los resultados para mostrarlos en pantalla
        txtResultado.setText("");
        if (c.moveToFirst()) {
            //Recorremos el cursor hasta que no encontrar más registros
            do {
                int idIndex = c.getColumnIndex("id");
                int nombreIndex = c.getColumnIndex("nombre");

                String cod = c.getString(idIndex);
                String nom = c.getString(nombreIndex);
                txtResultado.append(" " + cod + " - " + nom + "\n");
            } while(c.moveToNext());
        }
    }
}
