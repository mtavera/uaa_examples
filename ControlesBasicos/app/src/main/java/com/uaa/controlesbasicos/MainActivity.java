package com.uaa.controlesbasicos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    //main_activity.xml
    private TextView salida;
    private EditText entrada;
    private Button btnAceptar;

    //radio_buttons.xml
    private RadioGroup rg;
    private Button rb_btnAceptar;
    private int radioSeleccionado = -1;
    private String mensaje;

    //check_boxes.xml
    private CheckBox[] opciones;
    private Button cb_btnAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        //this.initMainActivity();
        //this.initCheckBoxActivity();
        this.initRadioActivity();
    }

    //inicializa layout "activity_main.xml"
    private void initMainActivity() {
        setContentView(R.layout.activity_main);
        this.entrada = (EditText) findViewById(R.id.edEntrada);
        this.salida = (TextView) findViewById(R.id.txSalida);
        this.btnAceptar = (Button)
                findViewById(R.id.btnAceptar);


        this.btnAceptar.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = entrada.getText().toString();
                salida.setText(texto);
            }
        });

    }

    private void initCheckBoxActivity() {
        //Definimos check_boxes.xml como la interfaz que
        // tendra nuestra Acitividad
        setContentView(R.layout.check_boxes);

        //Obtenemos referencia de los
        // controles dentro de check_boxex.xml
        this.cb_btnAceptar =
                (Button) findViewById(R.id.cb_btn_aceptar);
        //opciones constara de un array con los
        // checkboxes declarados en check_boxes.xml
        // (ios, android, winphone)
        this.opciones = new CheckBox[]{
                (CheckBox) findViewById(R.id.cb_ios),
                (CheckBox) findViewById(R.id.cb_android),
                (CheckBox) findViewById(R.id.cb_winphone)
        };

        //Definimos el escuchador del evento
        // cuando android detecte un click en
        // el boton dentro de check_boxes.xml
        // el metodo onClick sera llamado
        // automaticamente
        this.cb_btnAceptar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int opcSeleccionadas = 0;
                        for (int i = 0; i < opciones.length;
                             i++) {
                            if (opciones[i].isChecked()) {
                                opcSeleccionadas++;
                            }
                        }
                        Toast.makeText(MainActivity.this,
                                "Opciones seleccionadas: "
                                        + opcSeleccionadas,
                                Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void initRadioActivity() {
        //Definimos radio_buttons.xml como la
        //interfaz que tendra nuestra Actividad
        setContentView(R.layout.radio_buttons);

        //Obtenemos referencia de los controles
        // dentro de radio_buttons.xml
        this.rg = (RadioGroup)
                findViewById(R.id.radio_grupo);
        this.rb_btnAceptar =
                (Button)
                   findViewById(R.id.btn_radioAceptar);

        //Definimos el escuchador del evento,
        // cuando android detecte una nueva seleccion
        //dentro de nuestro radio grupo se mandara
        // llamar automaticamente el metodo
        //"onCheckedChanged"
        this.rg.setOnCheckedChangeListener(
                new RadioGroup.
                        OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged
                            (RadioGroup group,
                             int checkedId) {
                        //utilizamos radioSeleccionado
                        //como auxiliar,
                        //guardaremos el id
                        //del radio button seleccionado
                        radioSeleccionado = checkedId;
                    }
                });

        //Definimos el escuchador del evento,
        // cuando android detecte
        // un click en el boton
        //el metodo onClick sera llamado
        // automaticamente
        this.rb_btnAceptar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Dependiendo de la opcion
                        // seleccionado desplegaremos un
                        // mensaje distinto
                        switch (radioSeleccionado) {
                            case R.id.rb_cpp:
                                mensaje = "C++";
                                break;
                            case R.id.rb_ruby:
                                mensaje = "Ruby";
                                break;
                            case R.id.rb_java:
                                mensaje = "JAVA";
                                break;
                            default:
                                mensaje =
                                 "No has seleccionado nada";
                                break;
                        }
                        //Mostramos una notificacion "Toast"
                        // al usuario con la opcion seleccionada.
                        //El metodo make text necesita 3 parametros:
                        //   1.El Contexto de la aplicacion
                        //   2.Mensaje a mostrar
                        //   3.Duracion en milisegundos
                        // (Toast.LELNGTH_SHORT es una duracion corta)
                        //Es muy importante llamar el metodo show()
                        //de lo contrario no se mostrara la notificacion
                        //de tipo Toast
                        Toast.makeText(MainActivity.this,
                                "Seleccionaste: " + mensaje,
                                Toast.LENGTH_SHORT).show();
                    }

                });
    }
}
