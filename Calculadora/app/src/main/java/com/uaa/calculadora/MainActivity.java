package com.uaa.calculadora;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private int[] digitsIds = new int[]{
            R.id.bnt_0, R.id.bnt_1, R.id.bnt_2, R.id.bnt_3,
            R.id.bnt_4, R.id.bnt_5, R.id.bnt_6, R.id.bnt_7,
            R.id.bnt_8, R.id.bnt_9
    };

    private int[] operatorIds = new int[]{
            R.id.btn_c, R.id.btn_c, R.id.btn_x, R.id.btn_del,
            R.id.btn_div,
            R.id.btn_m, R.id.btn_p, R.id.btn_point, R.id.btn_eq
    };

    private EditText edtResult;
    private String lastOperator;
    private float[] digits = new float[2];
    private int index = 0;
    //private String TAG = "MainAcitivty";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.iniWidgets();
    }

    private void iniWidgets() {
        this.edtResult = (EditText) findViewById(R.id.edt_result);
        this.initDigitButtons();
        this.initOperatorButtons();
    }

    private void initDigitButtons() {
        View.OnClickListener digitButtonsListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newDigit = ((Button) view).getText().toString();
                String current = edtResult.getText().toString();
                if (isNumber(current)) {
                    edtResult.append(newDigit);
                } else {
                    edtResult.setText(newDigit);
                }
            }
        };
        for (int digitsId : this.digitsIds)
            findViewById(digitsId).setOnClickListener(digitButtonsListener);
    }

    private void initOperatorButtons() {
        View.OnClickListener operatorButtonsListener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String operator = ((Button) view).getText().toString();
                if (operator.equals("=")) {
                    saveDigit();
                    doOperation();
                } else {
                    lastOperator = operator;
                    switch (operator) {
                        case ".":
                            edtResult.append(operator);
                            break;
                        case "C":
                            removeLastCharacter();
                            break;
                        case "del":
                            edtResult.setText("");
                            digits[0] = 0f;
                            digits[1] = 0f;
                            lastOperator = "";
                            break;
                        default:
                            saveDigit();
                            break;
                    }
                }
            }
        };
        for (int operatorId : this.operatorIds)
            findViewById(operatorId).setOnClickListener(operatorButtonsListener);
    }

    private void doOperation() {
        switch (lastOperator) {
            case "+":
                edtResult.setText(String.valueOf(sum(digits[0], digits[1])));
                break;
            case "-":
                edtResult.setText(String.valueOf(minus(digits[0], digits[1])));
                break;
            case "x":
                edtResult.setText(String.valueOf(multiplication(digits[0], digits[1])));
                break;
            case "/":
                edtResult.setText(String.valueOf(division(digits[0], digits[1])));
                break;
        }
    }

    private boolean isNumber(String num) {
        try {
            float eval = Float.parseFloat(num);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private void saveDigit() {
        try {
            index = index % 2;
            //Log.d(TAG, "index: " + index);
            digits[index] = Float.parseFloat(edtResult.getText().toString());
            index++;
            edtResult.setText(lastOperator);
        } catch (NumberFormatException ignore) {
        }
    }

    private void removeLastCharacter() {
        StringBuilder text = new StringBuilder(edtResult.getText());
        text.setLength(text.length() - 1);
        edtResult.setText(text);
    }

    private float sum(float number1, float number2) {
        return number1 + number2;
    }

    private float minus(float number1, float number2) {
        return number1 - number2;
    }

    private float multiplication(float number1, float number2) {
        return number1 * number2;
    }

    private float division(float number1, float number2) {
        return number1 / number2;
    }
}
