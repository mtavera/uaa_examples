package com.uaa.multiplesactivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/***
 * Created by Mario1 on 12/09/16.
 */
public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle
                                    savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Intent intent = getIntent();
        String name = intent.getStringExtra("NAME");
        TextView textView =
                (TextView) findViewById(R.id.textView2);
        textView.setText(name);

        Button button = (Button) findViewById(R.id.button2);
        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Aqui llamamos a la otra actividad
                        Intent intent = new Intent(Activity2.this,
                                MainActivity.class);
                        //limpiar back stack de actividades
                        //SOLO FUNCIONA EN API 10+
                        /*intent.setFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                Intent.FLAG_ACTIVITY_NEW_TASK
                        );*/
                        startActivity(intent);
                        //destruir actividad
                        finish();
                    }
                });
    }
}
