package com.uaa.sharedpreferences.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.uaa.sharedpreferences.R;
import com.uaa.sharedpreferences.util.SharedPreferenceHelper;

public class MainActivity extends AppCompatActivity {

    private Button btnLoad;
    private Button btnSave;
    private Button btnDelete;
    private EditText editEmail;
    private EditText editFirstName;
    private EditText editLastName;
    private EditText editAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initViews();
        this.initListners();
    }

    private void initViews() {
        this.btnSave = (Button) findViewById(R.id.bnt_save);
        this.btnLoad = (Button) findViewById(R.id.btn_load);
        this.btnDelete = (Button) findViewById(R.id.btn_delete);
        this.editEmail = (EditText) findViewById(R.id.edt_email);
        this.editFirstName = (EditText) findViewById(R.id.edt_first_name);
        this.editLastName = (EditText) findViewById(R.id.edt_last_name);
        this.editAge = (EditText) findViewById(R.id.edt_age);
    }

    private void initListners() {
        View.OnClickListener l = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v == btnSave) {
                    saveInPreferences();
                } else if (v == btnLoad) {
                    loadPreferences();
                } else {
                    deletePreferences();
                }
            }
        };

        btnSave.setOnClickListener(l);
        btnLoad.setOnClickListener(l);
        btnDelete.setOnClickListener(l);
    }

    private void saveInPreferences() {
        String email = this.editEmail.getText().toString();
        SharedPreferenceHelper.putEmail(this, email);

        String firstName = this.editFirstName.getText().toString();
        SharedPreferenceHelper.putFirstName(this, firstName);

        String lastName = this.editLastName.getText().toString();
        SharedPreferenceHelper.putLastName(this, lastName);

        int age = Integer.parseInt(this.editAge.getText().toString());
        SharedPreferenceHelper.putAge(this, age);
    }

    private void loadPreferences() {
        String email = SharedPreferenceHelper.getEmail(this);
        this.editEmail.setText(email);

        String firstName = SharedPreferenceHelper.getFirstName(this);
        this.editFirstName.setText(firstName);

        String lastName = SharedPreferenceHelper.getLastName(this);
        this.editLastName.setText(lastName);

        int age = SharedPreferenceHelper.getAge(this);
        this.editAge.setText(String.valueOf(age));
    }

    private void deletePreferences() {
        SharedPreferences.Editor editor = SharedPreferenceHelper.getEditor(this);
        editor.clear();
        editor.commit();
    }

}
