package com.uaa.sharedpreferences.util;

import android.content.Context;
import android.content.SharedPreferences;

/***
 * Created by Mario1 on 12/11/16.
 */

public class SharedPreferenceHelper {

    private static String PREFERENCES_FILE_NAME = "MisPreferencias";
    private static final String EMAIL = "EMAIL";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String AGE = "AGE";

    public static SharedPreferences getSharedPreferences(Context context) {

        SharedPreferences prefs = context.getSharedPreferences(PREFERENCES_FILE_NAME,
                Context.MODE_PRIVATE);

        return prefs;
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();

        return editor;
    }


    public static void putEmail(Context context, String value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(EMAIL, value);
        editor.commit();
    }

    public static String getEmail(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        prefs.getString(EMAIL, "");
        return prefs.getString(EMAIL, "");
    }

    public static void putFirstName(Context context, String value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(FIRST_NAME, value);
        editor.commit();
    }

    public static String getFirstName(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        prefs.getString(FIRST_NAME, "");
        return prefs.getString(FIRST_NAME, "");
    }

    public static void putLastName(Context context, String value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(LAST_NAME, value);
        editor.commit();
    }

    public static String getLastName(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        prefs.getString(LAST_NAME, "");
        return prefs.getString(LAST_NAME, "");
    }

    public static void putAge(Context context, int value) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putInt(AGE, value);
        editor.commit();
    }

    public static int getAge(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getInt(AGE, -1);
    }


}
