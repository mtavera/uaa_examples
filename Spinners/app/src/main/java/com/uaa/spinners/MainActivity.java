package com.uaa.spinners;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Declaramos los controles
    // contenidos
    // en activity_main.xml
    //para poder utilizarlos mas delante
    private Spinner spinnerList1;
    private Spinner spinnerList2;

    //usaremos un atributo de tipo Context,
    //donde guardaremos el contexto
    // de nuestra aplicacion
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Obtenemos el contexto de
        //nuestra aplicacion
        this.context = MainActivity.this;

        //hacemos referencia a los controles
        //spinner_list declarado en activity_main.xml
        this.spinnerList1 =
                (Spinner) findViewById(R.id.spinner_list1);
        this.spinnerList2 =
                (Spinner) findViewById(R.id.spinner_list2);

        //Declaramos un array de tipo String
        // donde almacenaremos las opciones que
        //Se desplegaran dentro del Spinner
        final String[] LENGUAJES = {
                "JAVA",
                "PHP", "C", "C++"};

        //Creamos un Adaptador utilizando un vector
        // de java
        ArrayAdapter<String> adaptador1 =
                this.getAdapter(LENGUAJES);

        //Creamos un Adaptador utilizando un
        // recurso de tipo string-array
        // en values/strings.xml
        ArrayAdapter<CharSequence> adaptador2 =
                this.getAdapter(R.array.sa_lenguajes);

        //Ponemos al primer spinner el adaptador
        // creado desde el vector de java
        this.spinnerList1.setAdapter(adaptador1);

        //De igual manera al segundo spinner
        // pero ahora con el adaptador creado
        //desde el recurso string-array en values/strings.xml
        this.spinnerList2.setAdapter(adaptador2);

        //Registramos eventos
        this.spinnerList1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
            /**
             * Metodo llamado automaticamente al
             * seleccionar algun item
             *
             * @param parent  El AdapterView
             *                donde la seleccion ocurrio
             * @param view  La vista dentro del
             *              AdapterView que fue clickeado
             * @param position
             * La posicion del elemento que fue clickeado
             * @param id  El id del item seleccionado
             * */
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position,
                                       long id) {
                Toast.makeText(context, "Item seleccionado: "
                        + LENGUAJES[position],
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?>
                                                  parent) {
            }
        });


        this.spinnerList2.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position,
                                       long id) {
                Toast.makeText(context,
                        "Item seleccionado: "
                                + parent
                                    .getItemAtPosition(position)
                                    .toString(),
                        Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    /**
     * Metodo utilizado para obtener
     * un ArrayAdapter desde un
     * vector de Java.
     *
     * @param array String[]
     * @return ArrayAdapter<String>
     */
    private ArrayAdapter<String>
        getAdapter(String[] array) {
            return new ArrayAdapter<>(this.context,
                android.R.layout
                .simple_spinner_item, array);
    }

    /**
     * Sobrecarga de getAdapter(String[] array)
     * Metodo ultizado para obtener
     * un ArrayAdapter
     * desde un recurso string-array
     *
     * @param recurso;
     * @return ArrayAdapter<CharSequence>
     */
    private ArrayAdapter<CharSequence>
         getAdapter(int recurso) {
                return ArrayAdapter
                        .createFromResource(
                                this.context,
                                recurso,
                                android.R.layout
                .simple_spinner_dropdown_item);

    }

}
